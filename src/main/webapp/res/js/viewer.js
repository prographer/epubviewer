var spineList = null;
var toi = 0;
var contentUrl = "";
var scrollHeight = 0;
var fontSize = 1.0;

var viewer_panel = null;

$.mobile.page.prototype.options.domCache = true;
$(function () {
    /**
     * VIewer를 init 한다.
     */
    var init_viewer = function () {
        viewer_panel = $('.viewer-panel');
        if ($('#spineList').val() == undefined) return;


        spineList = JSON.parse($('#spineList').val());
        toi = parseInt($('#toi').val());
        contentUrl = $('#contentUrl').val();

        viewer_panel.css({
            "width": $(document).width() - 20/*left,right margin*/,
            "height": get_mobile_height() - 20/*top,bottom margin*/
        });
        $('.btn-pos, .search-results').css({
            "height": get_mobile_height()/*top,bottom margin*/
        });

        loadPage($('#initUrl').val(), function () {

        });
    };

    $(document).on("keypress", "#search", function (e) {
        if (e.keyCode == 13) {
            search();
        }

    });

    $(document).on("pageshow", ".viewer", function () {
        init_viewer();
    });
    $(document).on("click", '.viewer-panel', function () {
        $('.btn-pos').toggle();
    });
    $(document).on("click", ".pre-button", function () {
        PrePage();
    });
    $(document).on("click", ".next-button", function () {
        NextPage();
    });
    $(document).on("swipeleft", '.viewer-panel', function () {
        NextPage();
    });
    $(document).on("swiperight", '.viewer-panel', function () {
        PrePage();
    });

    //새로고침시에 발생
    init_viewer();

    $("#slider").attr('max', spineList.length);
    $(document).on("slidestop", "#slider", function () {
        toi = $(this).val();
        $('#toi').val(toi);
        loadPage(contentUrl + spineList[toi].href, function () {
        });
    });
});
var search = function () {
    var search = $("#search");
    $('.search-results ul').html('');
    $.ajax({
        url: "/viewer/search",
        type: 'POST',
        data: {
            "epub-url": $("#epub-url").val(),
            "s": search.val()
        },
        success: function (data) {
            var search_results = $('.search-results ul');
            $.each(data, function (idx, result) {
                $.each(result.searchPageInfoList, function (i, info) {
                    var infoText = info.text;

                    infoText = infoText.replace(search.val(), "<span class='search-results-highlight'>" + search.val() + "</span>")
                    search_results.append("<li onclick='loadSearchPage(\"" + result.href + "\"," + i + ",\"" + search.val() + "\")'><a href='#' class='ui-btn'>" + infoText + "</a></li>");
                });

            })
        }
    });
}

var convertContetns = function (data) {
    var updatedData = data.replace(/(Images|images)+/g, contentUrl + "$1");
    viewer_panel.html(updatedData);
    var links = viewer_panel.find("link");
    $.each(links, function (idx, link) {
        $(link).attr('href', contentUrl + $(link).attr('href'));
    });

    var alinks = viewer_panel.find("a");
    $.each(alinks, function (idx, a) {
        if ($(a).attr('href') !== undefined) {
            var href = contentUrl + $(a).attr('href').replace("..", "");
            $(a).attr('href', "#");
            $(a).data('href', href);
            $(a).attr('onclick', "loadPage('" + href + "')");
        }
    });

    scrollHeight = viewer_panel[0].scrollHeight - viewer_panel.offset().top;
}
var gotoSearchPage = function (searchVal, idx) {
    $('.search-val').replaceWith($('.search-val:first').contents());
    var tags = $('.viewer-panel :not(title,meta,link,script,img,style,iframe,input,object,option,blockquote,a)')

    var search_idx = 0;
    $.each(tags, function (i, tag) {
        var splitValues = $(tag).html().split(searchVal);
        var sv = splitValues[0];
        var secah_ok = true;
        for (var i = 1; i < splitValues.length; i++) {
            if (search_idx == idx) {
                sv = sv + "<span class='search-val'>" + searchVal + "</span>" + splitValues[i];
                secah_ok = false;//break;
            } else {
                sv = sv + searchVal + splitValues[i];
            }
            search_idx++;
        }
        $(tag).html(sv);
        return secah_ok;

    });

    viewer_panel.scrollTop(0);
    viewer_panel.scrollTop($('.search-val').position().top);
}
var loadSearchPage = function (url, idx, searchVal) {
    if ($('#current-url').val() == url) {
        gotoSearchPage(searchVal, idx);
        return;
    }
    $('#current-url').val(url);

    loadPage(url, function () {
        $.each(spineList, function (idx, data) {
            if (contentUrl + data.href == url) {
                toi = idx;
            }
        });
        gotoSearchPage(searchVal, idx);
        setOverlay();
    })
}
var loadPage = function (url, callback) {
    //현재 로드된 페이지 경로 추가
    $('#current-url').val(url);
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'Text',
        beforeSend: function () {
            $.mobile.loading("show");
        },
        success: function (data) {
            convertContetns(data);

            if (callback != undefined)
                callback();
            else {
                viewer_panel.scrollTop(0);
            }
            $('#slider').val(toi).slider("refresh");
            setOverlay();
            $.mobile.loading("hide");
        }
    });
}

var setOverlay = function () {

    var ov = $('.viewer-overlay');
    var vpOh = viewer_panel.offset().top;
    var h = viewer_panel.outerHeight() + vpOh;

    var topCheck = false;
    viewer_panel.children().each(function (idx, el) {
        if (!topCheck && $(el).offset().top + $(el).height() - parseFloat($(el).css('margin-bottom')) >= vpOh) {
            viewer_panel.scrollTop(viewer_panel.scrollTop() - (vpOh - $(el).offset().top));
            topCheck = true;
        }

        if ($(el).offset().top + $(el).height() > h + parseFloat($(el).css('margin-bottom'))) {
            /*var preEl = viewer_panel.children()[idx - 1];
            var ovH = h - ($(preEl).offset().top + $(preEl).height());*/
            var ovH = h - ($(el).offset().top + $(el).height());
            ov.height(ovH);
            if (ovH >= 0)
                ov.height(ovH);
            else
            {
                var preEl = viewer_panel.children()[idx - 1];
                ovH = h - ($(preEl).offset().top + $(preEl).height());
                if (ovH >= 0)
                    ov.height(ovH);
                else
                    ov.height(h - $(preEl).offset().top);
            }


            return false;
        }
    });
}

var NextPage = function () {
    var ov = $('.viewer-overlay');
    var nextPos = (viewer_panel.scrollTop() + viewer_panel.outerHeight()) - ov.height();
    ov.height(0);
    if (scrollHeight - nextPos <= 0) {
        if (spineList.length > toi) {
            $('#toi').val(toi++);
            loadPage(contentUrl + spineList[toi].href, function () {

            });
        }
    } else {
        viewer_panel.scrollTop(nextPos);

        if(scrollHeight - nextPos < viewer_panel.height()){
            viewer_panel.append($("<div></div>").css("height",viewer_panel.height()-(scrollHeight - nextPos)))
        }
        setOverlay();
    }

}
var PrePage = function () {
    var ov = $('.viewer-overlay');
    var prePos = (viewer_panel.scrollTop() - viewer_panel.outerHeight());// - ov.height();
    ov.height(0);
    if (viewer_panel.scrollTop() == 0) {
        if (toi > 0) {
            $('#toi').val(toi--);
            loadPage(contentUrl + spineList[toi].href, function () {
                viewer_panel.scrollTop(viewer_panel[0].scrollHeight);

            });

        }
    } else if (prePos < 0) {
        viewer_panel.scrollTop(0);
        setOverlay();
    } else {
        viewer_panel.scrollTop(prePos);
        setOverlay();
    }

}
var select_toc = function (idx, id, url) {
    toi = idx;
    $('#toi').val(toi);
    loadPage(url, function () {
        viewer_panel.scrollTop(0);

    });
    $("#table-of-context-panel").panel("close");
}


/**
 * 화면 높이를 갖고 온다.
 * @returns {number}
 */
var get_mobile_height = function () {
    var ui_header = $(".ui-header");
    var ui_footer = $(".ui-footer");
    var screen = $.mobile.getScreenHeight();
    var header = ui_header.hasClass("ui-header-fixed") ? ui_header.outerHeight() - 1 : ui_header.outerHeight();
    var footer = ui_footer.hasClass("ui-footer-fixed") ? ui_footer.outerHeight() - 1 : ui_footer.outerHeight();

    var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();
    var content = screen - header - footer - contentCurrent;

    return content;
}

var fontResize = function (type) {
    if (type == 0) {
        //plus
        fontSize += 0.1;
        viewer_panel.css('fontSize', fontSize + "em");
        viewer_panel.find("h1,h2,h3,h4,blockquote").each(function (idx, e) {
            var size = parseInt($(this).css("fontSize"));
            $(this).css("fontSize", size + 1.5);
        });
    } else {
        //minus
        fontSize -= 0.1;
        viewer_panel.css('fontSize', fontSize + "em");
        viewer_panel.find("h1,h2,h3,h4,blockquote").each(function (idx, e) {
            var size = parseInt($(this).css("fontSize"));
            $(this).css("fontSize", size - 1.5);
        });
    }
}