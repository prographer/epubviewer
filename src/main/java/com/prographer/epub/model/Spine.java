package com.prographer.epub.model;

/**
 * Spine 정보 모델
 */
public class Spine {
    String id;
    String href;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public Spine(String id, String href) {

        this.id = id;
        this.href = href;
    }
}
