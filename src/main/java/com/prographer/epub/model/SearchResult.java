package com.prographer.epub.model;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class SearchResult {
    ArrayList<SearchPageInfo> searchPageInfoList = new ArrayList<SearchPageInfo>();
    String href;

    public SearchResult(String href) {
        this.href = href;
    }

    public ArrayList<SearchPageInfo> getSearchPageInfoList() {
        return searchPageInfoList;
    }
    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }


}
