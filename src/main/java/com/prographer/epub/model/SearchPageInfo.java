package com.prographer.epub.model;

/**
 * Created by prographers on 2015-03-10.
 */

public class SearchPageInfo {
    int pos;
    String text;

    public SearchPageInfo(int pos, String text) {
        this.pos = pos;
        this.text = text;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
