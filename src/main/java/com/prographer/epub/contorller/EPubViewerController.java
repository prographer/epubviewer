package com.prographer.epub.contorller;

import com.prographer.epub.Constant;
import com.prographer.epub.model.SearchPageInfo;
import com.prographer.epub.model.SearchResult;
import com.prographer.epub.model.Spine;
import com.prographer.epub.model.Toc;
import com.prographer.epub.util.FileDownload;
import com.prographer.epub.util.ZipUtil;
import nl.siegmann.epublib.domain.Book;
import nl.siegmann.epublib.domain.SpineReference;
import nl.siegmann.epublib.domain.TOCReference;
import nl.siegmann.epublib.epub.EpubReader;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

@Controller
@RequestMapping("/viewer")
public class EPubViewerController {
    Logger log = LoggerFactory.getLogger(MainController.class);

    @RequestMapping(method = RequestMethod.POST)
    public String postView() {
        return "redirect:/";
    }


    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getView(
            @RequestParam(value = "epub-url") String fileUrl,
            HttpServletRequest request) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("viewer");
        mav.addObject("app-name", Constant.APP_NAME);
        mav.addObject("app-version", Constant.Version);
        mav.addObject("epub-url", fileUrl);
        if (fileUrl != null && !fileUrl.equals("")) {
            ServletContext context = request.getServletContext();
            String appPath = context.getRealPath("/epub-contents");
            String localFilePath = FileDownload.download(fileUrl, appPath);

            try {
                ZipUtil.unzip(localFilePath);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }

            /**
             * Epub Load
             */
            EpubReader reader = new EpubReader();
            try {
                Book book = reader.readEpub(new FileInputStream(localFilePath), "UTF-8");
                mav.addObject("book-title", book.getTitle());

               String unzip_url = localFilePath.replace(appPath, "");
                unzip_url = unzip_url.substring(1, unzip_url.lastIndexOf((".")));
                String root_href = book.getOpfResource().getHref().substring(0, book.getOpfResource().getHref().lastIndexOf("/") + 1);
                StringBuilder content_url = new StringBuilder();
                content_url.append("/epub-contents/").append(unzip_url).append("/").append(root_href);

                mav.addObject("book-root-href", root_href);
                mav.addObject("book-url", content_url.toString() + book.getCoverPage().getHref());

                //toc -> spec2에만 적용됨
                if (book.getTableOfContents().getTocReferences().size() > 0) {
                    ArrayList<Toc> tocList = new ArrayList<Toc>();
                    for (TOCReference toc : book.getTableOfContents().getTocReferences()) {
                        tocList.add(new Toc(toc.getResource().getId(), toc.getTitle(), content_url.toString() + toc.getResource().getHref()));
                    }
                    mav.addObject("tocList", tocList);

                    ArrayList<Spine> spineList = new ArrayList<Spine>();
                    for (SpineReference spine : book.getSpine().getSpineReferences()) {
                        spineList.add(new Spine(spine.getResource().getId(), spine.getResource().getHref()));
                    }
                    ObjectMapper om = new ObjectMapper();
                    mav.addObject("contentUrl", content_url);
                    String spineListJsonString = om.writeValueAsString(spineList);
                    mav.addObject("spineList", spineListJsonString);
                }


            } catch (IOException e) {
                log.error("Epub not read!", e);
            }

        }


        return mav;
    }

    public String getRootUrl(String localFilePath, String appPath, String bookOpfHref) {
        String unzip_url = localFilePath.replace(appPath, "");
        unzip_url = unzip_url.substring(1, unzip_url.lastIndexOf((".")));
        String root_href = bookOpfHref.substring(0, bookOpfHref.lastIndexOf("/") + 1);
        StringBuilder content_url = new StringBuilder();
        content_url.append("/epub-contents/").append(unzip_url).append("/").append(root_href);
        return content_url.toString();
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String Search(
            @RequestParam(value = "epub-url") String fileUrl,
            @RequestParam("s") String searchTerm,
            HttpServletRequest request) {
        if (fileUrl != null && !fileUrl.equals("")) {
            ServletContext context = request.getServletContext();
            String appPath = context.getRealPath("/epub-contents");
            String localFilePath = FileDownload.download(fileUrl, appPath);

            /**
             * Epub Load
             */
            EpubReader reader = new EpubReader();
            ArrayList<SearchResult> searchResults = new ArrayList<SearchResult>();
            try {
                Book book = reader.readEpub(new FileInputStream(localFilePath), "UTF-8");

                for (SpineReference sr : book.getSpine().getSpineReferences()) {

                    String data = new String(sr.getResource().getData(), "utf-8");
                    String href = getRootUrl(localFilePath, appPath, book.getOpfResource().getHref()) + sr.getResource().getHref();
                    SearchResult searchResult = new SearchResult(href);
                    //검색
                    for (int findPos = data.indexOf(searchTerm); findPos >= 0; findPos = data.indexOf(searchTerm, findPos + 1)) {
                        int endPos = findPos + searchTerm.length() + 5;
                        int prePos = findPos - 5;
                        if (prePos < 0)
                            prePos = findPos;
                        else {
                            if (data.indexOf(">", prePos) < findPos) {
                                prePos = data.indexOf(">", prePos) + 1;
                            }
                        }
                        if (data.indexOf("<", findPos) < endPos) {
                            endPos = data.indexOf("<", findPos);
                        }
                        String txt = endPos == findPos + searchTerm.length() ? data.substring(prePos, endPos) : data.substring(prePos, endPos) + "...";
                        if (!data.substring(prePos - 1, prePos).equals(">"))
                            txt = "..." + txt;
                        searchResult.getSearchPageInfoList().add(new SearchPageInfo(prePos, txt));
                    }

                    if (searchResult.getSearchPageInfoList().size() > 0) {
                        searchResults.add(searchResult);
                    }
                }
                ObjectMapper om = new ObjectMapper();

                return om.writeValueAsString(searchResults);
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return null;
    }


}